import numpy as np
from sklearn.linear_model import LinearRegression
import scipy.signal as sps
import matplotlib.pyplot as plt
import pandas as pd


class SwipProcessing:
    __min_scale_factor_percentage = 100
    __max_scale_factor_percentage = 160
    __particles_left_calibration = 0
    __step_percentage = 1
    __estimate_param = True
    __reg = 0
    __db_indicatrix = []
    db_indicatrix_mean = []
    __db_param = []
    __score_limit = 0.85
    particle_detect = False
    db_index = 0
    score = 0
    bias = 0
    scale_factor = 0


    def fit_measurement(self, measure):

        model_indicatrix = self.__db_indicatrix[self.db_index][:]
        resample_indicatrix = sps.resample(measure,
                                           int(self.scale_factor/100*len(model_indicatrix)))

        measurement_indicatrix = resample_indicatrix[self.bias:self.bias + len(model_indicatrix)]
        reg = LinearRegression().fit(measurement_indicatrix.reshape((-1, 1)), model_indicatrix)
        print(reg.score(measurement_indicatrix.reshape((-1, 1)), model_indicatrix))
        print("scale factor:", self.scale_factor )
        print("bias:", self.bias)
        return reg.predict(measurement_indicatrix.reshape((-1, 1)))

    def get_ideal_signal(self):
        return self.__db_indicatrix[self.db_index][:]

    def get_param_signal(self):
        return self.__db_param[self.db_index][:]

    def get_signal_score(self):
        return self.score

    def plot(self, measure_indicatrix):

        model_indicatrix = self.__db_indicatrix[self.db_index][2:]

        resample_indicatrix = sps.resample(measure_indicatrix,
                                           int(self.scale_factor/100*len(model_indicatrix)))

        measurement_indicatrix = resample_indicatrix[self.bias:self.bias + len(model_indicatrix)]

        reg = LinearRegression().fit(measurement_indicatrix.reshape((-1, 1)), model_indicatrix)
        print(reg.score(measurement_indicatrix.reshape((-1, 1)), model_indicatrix))


        plt.plot(reg.predict(measurement_indicatrix.reshape((-1, 1))))
        plt.plot(model_indicatrix)
        plt.show()