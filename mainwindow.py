import argparse
import csv
import sys
import threading
import time
import signal
import os
from PySide6.QtWidgets import QApplication, QMessageBox
from datetime import datetime
import json
from collections import deque
from time import perf_counter
import numpy as np
import qdarkstyle  # noqa: E402
from qdarkstyle.dark.palette import DarkPalette  # noqa: E402
from qdarkstyle.light.palette import LightPalette
from PySide6.QtWidgets import QApplication, QMainWindow, QRadioButton, QInputDialog, QProgressBar
from PySide6 import QtCore, QtGui, QtWidgets
from PySide6.QtGui import QFont, QColor
from PySide6.QtCore import QCoreApplication
import pyqtgraph as pg
import pyqtgraph.functions as fn
from algorithm_bands import AlgorithmBands
from monkey_curve_item import MonkeyCurveItem
from rp_data_acquired import RpDataAcquired
from ui_form import Ui_MainWindow
import pandas as pd


# Create the MonkeyCurveItem class outside the MainWindow class
class MainWindow(QMainWindow):
    clear_data = pg.QtCore.Signal()

    def __init__(self, parent=None):
        super().__init__(parent)
        self.ui = Ui_MainWindow()
        self.ui.setupUi(self)
        # change a dark theme
        self.ui.radioButton.toggled.connect(self.toggle_theme)

        self.spectrum_ch = [[], [], []]
        self.absorption_ch = [[], [], []]

        # Create the MonkeyCurveItems for pw_calculated and pw_real_time
        self.curve_counter = []
        self.co2_concentration = []
        self.curve_temperature = []
        self.curve_temp = []
        self.curve_humidity = []

        self.curve_counter.append(MonkeyCurveItem(pen=pg.mkPen({'width': 1, 'color': "#FFFFFF"}), brush='b'))
        self.curve_counter.append(MonkeyCurveItem(pen=pg.mkPen({'width': 4, 'color': "#0000FF"}), brush='b'))
        self.curve_counter.append(MonkeyCurveItem(pen=pg.mkPen({'width': 4, 'color': "#00FF00"}), brush='b'))
        self.co2_concentration.append(MonkeyCurveItem(pen=pg.mkPen({'color': "#FFFFFF"}), brush='b'))
        self.co2_concentration.append(MonkeyCurveItem(pen=pg.mkPen({'color': "#0000FF"}), brush='b'))
        self.co2_concentration.append(MonkeyCurveItem(pen=pg.mkPen({'color': "#00FF00"}), brush='b'))

        self.curve_temperature.append(MonkeyCurveItem(pen=pg.mkPen({'width': 1, 'color': "#FFFFFF"}), brush='b'))
        self.curve_humidity.append(MonkeyCurveItem(pen=pg.mkPen({'width': 1, 'color': "#FFFFFF"}), brush='b'))

        # self.pw_real_time = pg.PlotWidget()
        # self.pw_temp = pg.PlotWidget()
        # self.pw_sperometr = pg.PlotWidget()
        # self.ui.verticalLayout_7.addWidget(self.pw_sperometr)
        # self.ui.verticalLayout_5.addWidget(self.pw_temp)
        # self.ui.verticalLayout_3.addWidget(self.pw_real_time)

        self.pw_counter = self.ui.pw_temp
        self.pw_co2_concentration = self.ui.pw_real_time
        self.pw_temperature = self.ui.pw_temperature
        self.pw_humidity = self.ui.pw_humidity
        self.pw_temperature.setYRange(20, 40)
        self.pw_humidity.setYRange(30, 100)

        self.pw_temperature.addItem(self.curve_temperature[0])
        self.pw_temperature.showGrid(x=True, y=True, alpha=0.3)

        self.pw_humidity.addItem(self.curve_humidity[0])
        self.pw_humidity.showGrid(x=True, y=True, alpha=0.3)

        self.pw_counter.addItem(self.curve_counter[0])
        self.pw_counter.addItem(self.curve_counter[1])
        self.pw_counter.addItem(self.curve_counter[2])

        self.pw_counter.showGrid(x=True, y=True, alpha=0.3)
        self.pw_counter.getAxis('left').setLabel("Rotations", color='white', size='12pt')
        self.pw_counter.getAxis('bottom').setLabel("Sample, index", color='white', size='12pt')
        # set channels current

        self.ui.spinBox_set_timer.setValue(10)

        self.pw_co2_concentration.showGrid(x=True, y=True, alpha=0.3)
        self.pw_co2_concentration.addItem(self.co2_concentration[0])
        self.pw_co2_concentration.addItem(self.co2_concentration[1])
        self.pw_co2_concentration.addItem(self.co2_concentration[2])
        self.pw_co2_concentration.setYRange(-0.5, 10)
        self.ui.pushButton_start_record.clicked.connect(self.pushStart)
        self.ui.pushButton_stop_record.clicked.connect(self.pushStop)
        self.ui.pushButton_export.clicked.connect(self.pushExport)
        self.pbar = QProgressBar(parent=None)
        self.ui.verticalLayout_6.addWidget(self.pbar)

        light_palette = DarkPalette()
        self.setStyleSheet(qdarkstyle.load_stylesheet(palette=light_palette))
        self.pw_co2_concentration.setBackground(light_palette.COLOR_BACKGROUND_1)
        self.pw_counter.setBackground(light_palette.COLOR_BACKGROUND_1)
        self.pw_humidity.setBackground(light_palette.COLOR_BACKGROUND_1)
        self.pw_temperature.setBackground(light_palette.COLOR_BACKGROUND_1)

        self.counter = 0
        self.current_time = 0
        self.start_index = 0
        self.end_index = 0

        self.flag_start_record = False
        self.flag_can_export = True
        self.ui.pushButton_export.setEnabled(False)
        # Set the signal handler for SIGALRM (timer interrupt)
        # Schedule the next call to this function
        threading.Timer(1, self.timer_handler).start()

    def timer_handler(self):
        if self.flag_start_record:
            self.current_time += 1

    def toggle_theme(self, checked):
        if checked:
            palette = DarkPalette()
            self.curve_temperature[0].setPen(pg.mkPen({'color': "#FFFFFF"}))
            self.curve_counter[0].setPen(pg.mkPen({'color': "#FFFFFF"}))
            self.curve_counter[1].setPen(pg.mkPen({'color': "#00FF00"}))
            self.co2_concentration[1].setPen(pg.mkPen({'color': "#00FF00"}))
            # Create the MonkeyCurveItems for pw_calculated and pw_real_time
        else:
            palette = LightPalette()
            self.curve_temperature[0].setPen(pg.mkPen({'color': "#FFFFFF"}))
            self.curve_counter[0].setPen(pg.mkPen({'color': "#FFFFFF"}))
            self.curve_counter[1].setPen(pg.mkPen({'color': "#00003F"}))
            self.co2_concentration[1].setPen(pg.mkPen({'color': "#00003F"}))

        self.setStyleSheet(qdarkstyle.load_stylesheet(palette=palette))
        self.pw_humidity.setBackground(palette.COLOR_BACKGROUND_1)
        self.pw_temperature.setBackground(palette.COLOR_BACKGROUND_1)
        self.pw_co2_concentration.setBackground(palette.COLOR_BACKGROUND_1)
        self.pw_counter.setBackground(palette.COLOR_BACKGROUND_1)

    def update_status_bar(self, text):
        self.ui.statusbar.showMessage(text)

    def update_signals(self, data_ch, channel):
        if channel == 3:
            self.curve_counter[0].setData(data_ch)
        elif channel == 2:
            self.curve_temperature[0].setData(data_ch)
        elif channel == 1:
            self.curve_humidity[0].setData(data_ch)
        else:
            self.co2_concentration[0].setData(data_ch)

        if self.flag_start_record:
            self.pbar.setValue(100 * self.current_time / self.counter)

            if self.current_time > self.counter:
                self.flag_start_record = False
                self.counter = 0
                self.pbar.setValue(100)
                self.ui.spinBox_set_timer.setEnabled(True)
                self.ui.pushButton_stop_record.setDisabled(True)
                self.ui.pushButton_start_record.setEnabled(True)
                self.ui.pushButton_export.setEnabled(True)
        return True

    def pushExport(self):
        # Get the current directory where the Python script is located
        current_directory = os.path.dirname(os.path.abspath(__file__))
        # Specify the folder name you want to create
        # Get the current timestamp as a string
        folder_name = datetime.now().strftime("%Y-%m-%d_%H-%M-%S")
        # Create the folder in the current directory
        folder_path = os.path.join(current_directory, folder_name)
        try:
            os.mkdir(folder_path)
            print(f"Folder '{folder_name}' created successfully in the current directory.")
        except OSError as e:
            print(f"Error creating folder: {e}")

        # Specify the CSV file path
        csv_file_path = folder_path + '/' + 'data.csv'
        co2_concentration = np.array(self.co2_concentration[0].getData()[1])
        temperature = np.array(self.curve_temperature[0].getData()[1])
        humidity = np.array(self.curve_humidity[0].getData()[1])
        counter = np.array(self.curve_counter[0].getData()[1])
        # save data in csv
        # Step 1: Combine the data into a dictionary
        data = {
            'CO2 Concentration': co2_concentration,
            'Temperature': temperature,
            'Humidity': humidity,
            'Counter': counter
        }

        # Step 2: Create a pandas DataFrame
        df = pd.DataFrame(data)
        df.to_csv(csv_file_path, index=False)  # Set index=False to avoid an unwanted column of row indices

        print(f"Data saved to {csv_file_path}")

        # Sample data as a Python dictionary
        data = {
            "Patient ID": self.ui.lineEdit_patient_id.text(),
            "Age": self.ui.spinBox_age.value(),
            "Sex": self.ui.comboBox_sex.currentText(),
            "Habits": self.ui.comboBox_habits.currentText(),
            "period_measurements": 20
        }

        # Specify the file path where you want to save the JSON data
        file_path = folder_path + '/' + "metadata.json"

        # Open the file in write mode and use json.dump() to save the data
        with open(file_path, 'w') as json_file:
            json.dump(data, json_file)

        # Save the data to a CSV file
        msg_box = QMessageBox()
        # Set the message text and other properties
        msg_box.setText("Data stored in folder: " + folder_name)  # Message text
        msg_box.setWindowTitle("Data exported successfully!")  # Title of the message box
        msg_box.setIcon(QMessageBox.Information)  # Icon type (Information in this case)

        # Add standard buttons to the message box (e.g., OK button)
        msg_box.addButton(QMessageBox.Ok)
        # Show the message box and wait for a response
        response = msg_box.exec()
        self.ui.pushButton_export.setDisabled(True)

    def pushStart(self):
        self.current_time = 0
        self.clear_data.emit()
        self.counter = self.ui.spinBox_set_timer.value()
        self.flag_start_record = True
        self.ui.pushButton_start_record.setDisabled(True)
        self.ui.pushButton_stop_record.setEnabled(True)
        self.ui.spinBox_set_timer.setDisabled(True)

    def pushStop(self):
        self.flag_start_record = False
        self.ui.spinBox_set_timer.setEnabled(True)
        self.ui.pushButton_stop_record.setDisabled(True)
        self.ui.pushButton_start_record.setEnabled(True)


class AcqThread(pg.QtCore.QThread):
    clear_data = pg.QtCore.Signal()
    new_data_update = pg.QtCore.Signal(object, object)
    connection_change = pg.QtCore.Signal(object)
    connectionError = pg.QtCore.Signal(str)

    def __init__(self, _index_rp, _ip_address):
        super(AcqThread, self).__init__()
        self.stopMutex = threading.Lock()
        self._stop = False
        self.raw_signal_ch1 = []
        self.raw_signal_ch2 = []
        self.connection_active = False
        self.rp_data_acquired = None  # Initialize the variable
        self.exit_event = threading.Event()
        self.ip_address = _ip_address
        self.index_rp = _index_rp
        self.lastUpdate = time.time()

    def connect_to_spherocap(self):
        ip_list = []
        ip_list.append(self.ip_address)
        for ip in ip_list:
            try:
                # Attempt to connect with a timeout of 0.2 seconds
                str = 'Connection ...' + ip
                self.connection_change.emit(str)
                self.rp_data_acquired = RpDataAcquired(ip)
                # If the connection is successful, break out of the loop
                str = 'Connected ' + ip
                self.connection_change.emit(str)
                self.connection_active = True
                return
            except Exception as e:
                # Emit a signal to indicate the connection error
                self.connection_change.emit('Connection Error')
                time.sleep(1)

    def clear_data(self):
        self.rp_data_acquired.clear_data()

    def data_processing(self):
        if self.rp_data_acquired.update():
            if (time.time() - self.lastUpdate) > 0.10:
                self.new_data_update.emit(self.rp_data_acquired.get_avg_value_channel_0(), 0)
                self.new_data_update.emit(self.rp_data_acquired.get_humidity_bme(), 1)
                self.new_data_update.emit(self.rp_data_acquired.get_temp_bme(), 2)

                self.new_data_update.emit(self.rp_data_acquired.get_pulse_count(), 3)
                self.lastUpdate = time.time()

    def run(self):
        while not self.exit_event.is_set():
            if self.connection_active:
                try:
                    self.data_processing()
                except Exception as e:
                    self.connection_active = False
                    self.connectionError.emit('Connection Error')
            else:
                self.connect_to_spherocap()  # Initial connection attempt

        # Close the connection or do any cleanup if necessary
        if self.rp_data_acquired is not None:
            print('Closing connection to Red Pitaya index:', self.index_rp)
            self.rp_data_acquired.close_thread()

    def stop(self):
        self.exit_event.set()
        self.wait()


if __name__ == "__main__":
    app = QApplication(sys.argv)
    exit_event = threading.Event()
    widget = MainWindow()

    acqThread = AcqThread(0, "/dev/cu.usbmodem_dx3651")

    acqThread.new_data_update.connect(widget.update_signals)
    acqThread.connection_change.connect(widget.update_status_bar)  # Connect the signal to the update function
    widget.clear_data.connect(acqThread.clear_data)

    acqThread.start()

    widget.show()
    # Connect the stop signal to the thread's stop method
    app.aboutToQuit.connect(acqThread.stop)

    sys.exit(app.exec())
