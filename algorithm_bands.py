import sys
import os
import numpy as np
import math
from scipy.integrate import simps


class AlgorithmBands:
    # extend line detection region
    extend_limits = 60
    def __init__(self, leftPeakIndx, rightPeakIndx):

        if leftPeakIndx > rightPeakIndx:
            self.leftPeakIndx = rightPeakIndx
            self.rightPeakIndx = leftPeakIndx
        else:
            self.leftPeakIndx = leftPeakIndx
            self.rightPeakIndx = rightPeakIndx

        self.centrPeakIndx = 0
        self.integralMax = 0
        self.integral = 0
        self.leftExpectIndx = 0
        self.leftLimitIndx = 0
        self.rightExpectIndx = 0
        self.rightLimitIndx = 0
        self.absorption = 0
        self.width = 0

    def peak_init(self, leftIndx, rightIndx):
        return {'centrPeakIndx': 0,
                'integralMax': 0.0,
                'integral': 0.0,
                'leftExpectIndx': 0,
                'leftLimitIndx': leftIndx,
                'leftPeakIndx': 0,
                'rightExpectIndx': 0,
                'rightLimitIndx': rightIndx,
                'rightPeakIndx': 0,
                'absorption': 0,
                'width': 0
                }

    def get_integral(self, spectrum):
        y = []
        y.extend(spectrum[self.leftExpectIndx:self.leftPeakIndx])
        y.extend(spectrum[self.rightPeakIndx:self.rightExpectIndx])
        x = []
        x.extend(range(self.leftExpectIndx, self.leftPeakIndx))
        x.extend(range(self.rightPeakIndx, self.rightExpectIndx))
        p = np.polyfit(x, y, 1)
        p = np.poly1d(p)
        line = p(range(self.leftPeakIndx, self.rightPeakIndx))
        self.integralMax = float(simps(line, dx=1))
        self.integral = float(simps(spectrum[self.leftPeakIndx:self.rightPeakIndx] - line, dx=1))
        self.absorption = self.integral/self.integralMax

    def process_band(self, spectrum):
        # not enough length
        if abs(self.leftPeakIndx - self.rightPeakIndx) < 10:
            return

        if self.leftPeakIndx > AlgorithmBands.extend_limits:
            left_expect = int(self.leftPeakIndx) - AlgorithmBands.extend_limits
        else:
            left_expect = 0
        if len(spectrum) - 1 > AlgorithmBands.extend_limits + self.rightPeakIndx:
            right_expect = int(self.rightPeakIndx) + AlgorithmBands.extend_limits
        else:
            right_expect = len(spectrum) - 1
        self.leftExpectIndx = left_expect
        self.rightExpectIndx = right_expect
        self.get_integral(spectrum)


