# -*- coding: utf-8 -*-

################################################################################
## Form generated from reading UI file 'form.ui'
##
## Created by: Qt User Interface Compiler version 6.5.1
##
## WARNING! All changes made in this file will be lost when recompiling UI file!
################################################################################

from PySide6.QtCore import (QCoreApplication, QDate, QDateTime, QLocale,
    QMetaObject, QObject, QPoint, QRect,
    QSize, QTime, QUrl, Qt)
from PySide6.QtGui import (QAction, QBrush, QColor, QConicalGradient,
    QCursor, QFont, QFontDatabase, QGradient,
    QIcon, QImage, QKeySequence, QLinearGradient,
    QPainter, QPalette, QPixmap, QRadialGradient,
    QTransform)
from PySide6.QtWidgets import (QApplication, QComboBox, QFormLayout, QGridLayout,
    QGroupBox, QHBoxLayout, QLabel, QLineEdit,
    QMainWindow, QMenu, QMenuBar, QPushButton,
    QRadioButton, QSizePolicy, QSpacerItem, QSpinBox,
    QSplitter, QStatusBar, QVBoxLayout, QWidget)

from pyqtgraph import PlotWidget

class Ui_MainWindow(object):
    def setupUi(self, MainWindow):
        if not MainWindow.objectName():
            MainWindow.setObjectName(u"MainWindow")
        MainWindow.resize(872, 718)
        self.centralwidget = QWidget(MainWindow)
        self.centralwidget.setObjectName(u"centralwidget")
        self.centralwidget.setLayoutDirection(Qt.LeftToRight)
        self.horizontalLayout_2 = QHBoxLayout(self.centralwidget)
        self.horizontalLayout_2.setObjectName(u"horizontalLayout_2")
        self.widget_2 = QWidget(self.centralwidget)
        self.widget_2.setObjectName(u"widget_2")
        self.widget_2.setMinimumSize(QSize(220, 0))
        self.widget_2.setMaximumSize(QSize(290, 16777215))
        self.verticalLayout = QVBoxLayout(self.widget_2)
        self.verticalLayout.setSpacing(0)
        self.verticalLayout.setObjectName(u"verticalLayout")
        self.verticalLayout.setContentsMargins(0, 0, 0, 6)
        self.groupBox = QGroupBox(self.widget_2)
        self.groupBox.setObjectName(u"groupBox")
        self.groupBox.setMinimumSize(QSize(0, 0))
        self.gridLayout_2 = QGridLayout(self.groupBox)
#ifndef Q_OS_MAC
        self.gridLayout_2.setSpacing(-1)
#endif
        self.gridLayout_2.setObjectName(u"gridLayout_2")
        self.gridLayout_2.setContentsMargins(6, 6, 6, 6)
        self.spinBox_end_band_ch3 = QSpinBox(self.groupBox)
        self.spinBox_end_band_ch3.setObjectName(u"spinBox_end_band_ch3")
        sizePolicy = QSizePolicy(QSizePolicy.Expanding, QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.spinBox_end_band_ch3.sizePolicy().hasHeightForWidth())
        self.spinBox_end_band_ch3.setSizePolicy(sizePolicy)
        self.spinBox_end_band_ch3.setMinimumSize(QSize(0, 0))
        self.spinBox_end_band_ch3.setMaximumSize(QSize(60, 16777215))
        self.spinBox_end_band_ch3.setMaximum(10000)

        self.gridLayout_2.addWidget(self.spinBox_end_band_ch3, 8, 1, 1, 1)

        self.label_9 = QLabel(self.groupBox)
        self.label_9.setObjectName(u"label_9")

        self.gridLayout_2.addWidget(self.label_9, 1, 0, 1, 1)

        self.label_21 = QLabel(self.groupBox)
        self.label_21.setObjectName(u"label_21")

        self.gridLayout_2.addWidget(self.label_21, 4, 2, 1, 1)

        self.label_11 = QLabel(self.groupBox)
        self.label_11.setObjectName(u"label_11")

        self.gridLayout_2.addWidget(self.label_11, 4, 0, 1, 1)

        self.label_23 = QLabel(self.groupBox)
        self.label_23.setObjectName(u"label_23")

        self.gridLayout_2.addWidget(self.label_23, 7, 2, 1, 1)

        self.spinBox_start_band_ch3 = QSpinBox(self.groupBox)
        self.spinBox_start_band_ch3.setObjectName(u"spinBox_start_band_ch3")
        sizePolicy.setHeightForWidth(self.spinBox_start_band_ch3.sizePolicy().hasHeightForWidth())
        self.spinBox_start_band_ch3.setSizePolicy(sizePolicy)
        self.spinBox_start_band_ch3.setMinimumSize(QSize(0, 0))
        self.spinBox_start_band_ch3.setMaximumSize(QSize(60, 16777215))
        self.spinBox_start_band_ch3.setMaximum(10000)

        self.gridLayout_2.addWidget(self.spinBox_start_band_ch3, 7, 1, 1, 1)

        self.spinBox_start_i_ch3_2 = QSpinBox(self.groupBox)
        self.spinBox_start_i_ch3_2.setObjectName(u"spinBox_start_i_ch3_2")
        sizePolicy.setHeightForWidth(self.spinBox_start_i_ch3_2.sizePolicy().hasHeightForWidth())
        self.spinBox_start_i_ch3_2.setSizePolicy(sizePolicy)
        self.spinBox_start_i_ch3_2.setMaximumSize(QSize(60, 16777215))

        self.gridLayout_2.addWidget(self.spinBox_start_i_ch3_2, 8, 3, 1, 1)

        self.spinBox_end_band_ch2 = QSpinBox(self.groupBox)
        self.spinBox_end_band_ch2.setObjectName(u"spinBox_end_band_ch2")
        sizePolicy.setHeightForWidth(self.spinBox_end_band_ch2.sizePolicy().hasHeightForWidth())
        self.spinBox_end_band_ch2.setSizePolicy(sizePolicy)
        self.spinBox_end_band_ch2.setMinimumSize(QSize(0, 0))
        self.spinBox_end_band_ch2.setMaximumSize(QSize(60, 16777215))
        self.spinBox_end_band_ch2.setMaximum(10000)

        self.gridLayout_2.addWidget(self.spinBox_end_band_ch2, 5, 1, 1, 1)

        self.label_15 = QLabel(self.groupBox)
        self.label_15.setObjectName(u"label_15")
        font = QFont()
        font.setBold(True)
        self.label_15.setFont(font)

        self.gridLayout_2.addWidget(self.label_15, 0, 0, 1, 1)

        self.label_24 = QLabel(self.groupBox)
        self.label_24.setObjectName(u"label_24")

        self.gridLayout_2.addWidget(self.label_24, 8, 2, 1, 1)

        self.label_4 = QLabel(self.groupBox)
        self.label_4.setObjectName(u"label_4")

        self.gridLayout_2.addWidget(self.label_4, 2, 2, 1, 1)

        self.spinBox_end_band_ch1 = QSpinBox(self.groupBox)
        self.spinBox_end_band_ch1.setObjectName(u"spinBox_end_band_ch1")
        sizePolicy.setHeightForWidth(self.spinBox_end_band_ch1.sizePolicy().hasHeightForWidth())
        self.spinBox_end_band_ch1.setSizePolicy(sizePolicy)
        self.spinBox_end_band_ch1.setMinimumSize(QSize(0, 0))
        self.spinBox_end_band_ch1.setMaximumSize(QSize(60, 16777215))
        self.spinBox_end_band_ch1.setMaximum(10000)
        self.spinBox_end_band_ch1.setValue(9800)

        self.gridLayout_2.addWidget(self.spinBox_end_band_ch1, 2, 1, 1, 1)

        self.spinBox_start_band_ch2 = QSpinBox(self.groupBox)
        self.spinBox_start_band_ch2.setObjectName(u"spinBox_start_band_ch2")
        sizePolicy.setHeightForWidth(self.spinBox_start_band_ch2.sizePolicy().hasHeightForWidth())
        self.spinBox_start_band_ch2.setSizePolicy(sizePolicy)
        self.spinBox_start_band_ch2.setMinimumSize(QSize(0, 0))
        self.spinBox_start_band_ch2.setMaximumSize(QSize(60, 16777215))
        self.spinBox_start_band_ch2.setMaximum(10000)

        self.gridLayout_2.addWidget(self.spinBox_start_band_ch2, 4, 1, 1, 1)

        self.spinBox_start_i_ch1 = QSpinBox(self.groupBox)
        self.spinBox_start_i_ch1.setObjectName(u"spinBox_start_i_ch1")
        sizePolicy.setHeightForWidth(self.spinBox_start_i_ch1.sizePolicy().hasHeightForWidth())
        self.spinBox_start_i_ch1.setSizePolicy(sizePolicy)
        self.spinBox_start_i_ch1.setMaximumSize(QSize(60, 16777215))

        self.gridLayout_2.addWidget(self.spinBox_start_i_ch1, 1, 3, 1, 1)

        self.spinBox_end_i_ch1 = QSpinBox(self.groupBox)
        self.spinBox_end_i_ch1.setObjectName(u"spinBox_end_i_ch1")
        sizePolicy.setHeightForWidth(self.spinBox_end_i_ch1.sizePolicy().hasHeightForWidth())
        self.spinBox_end_i_ch1.setSizePolicy(sizePolicy)
        self.spinBox_end_i_ch1.setMaximumSize(QSize(60, 16777215))

        self.gridLayout_2.addWidget(self.spinBox_end_i_ch1, 2, 3, 1, 1)

        self.label_18 = QLabel(self.groupBox)
        self.label_18.setObjectName(u"label_18")
        self.label_18.setFont(font)

        self.gridLayout_2.addWidget(self.label_18, 3, 0, 1, 1)

        self.label_10 = QLabel(self.groupBox)
        self.label_10.setObjectName(u"label_10")

        self.gridLayout_2.addWidget(self.label_10, 2, 0, 1, 1)

        self.label_16 = QLabel(self.groupBox)
        self.label_16.setObjectName(u"label_16")

        self.gridLayout_2.addWidget(self.label_16, 8, 0, 1, 1)

        self.label_17 = QLabel(self.groupBox)
        self.label_17.setObjectName(u"label_17")
        self.label_17.setFont(font)

        self.gridLayout_2.addWidget(self.label_17, 6, 0, 1, 1)

        self.label_14 = QLabel(self.groupBox)
        self.label_14.setObjectName(u"label_14")

        self.gridLayout_2.addWidget(self.label_14, 7, 0, 1, 1)

        self.spinBox_start_i_ch2_2 = QSpinBox(self.groupBox)
        self.spinBox_start_i_ch2_2.setObjectName(u"spinBox_start_i_ch2_2")
        sizePolicy.setHeightForWidth(self.spinBox_start_i_ch2_2.sizePolicy().hasHeightForWidth())
        self.spinBox_start_i_ch2_2.setSizePolicy(sizePolicy)
        self.spinBox_start_i_ch2_2.setMaximumSize(QSize(60, 16777215))

        self.gridLayout_2.addWidget(self.spinBox_start_i_ch2_2, 5, 3, 1, 1)

        self.spinBox_start_i_ch2 = QSpinBox(self.groupBox)
        self.spinBox_start_i_ch2.setObjectName(u"spinBox_start_i_ch2")
        sizePolicy.setHeightForWidth(self.spinBox_start_i_ch2.sizePolicy().hasHeightForWidth())
        self.spinBox_start_i_ch2.setSizePolicy(sizePolicy)
        self.spinBox_start_i_ch2.setMaximumSize(QSize(60, 16777215))

        self.gridLayout_2.addWidget(self.spinBox_start_i_ch2, 4, 3, 1, 1)

        self.label_13 = QLabel(self.groupBox)
        self.label_13.setObjectName(u"label_13")

        self.gridLayout_2.addWidget(self.label_13, 5, 0, 1, 1)

        self.label_22 = QLabel(self.groupBox)
        self.label_22.setObjectName(u"label_22")

        self.gridLayout_2.addWidget(self.label_22, 5, 2, 1, 1)

        self.spinBox_start_band_ch1 = QSpinBox(self.groupBox)
        self.spinBox_start_band_ch1.setObjectName(u"spinBox_start_band_ch1")
        sizePolicy.setHeightForWidth(self.spinBox_start_band_ch1.sizePolicy().hasHeightForWidth())
        self.spinBox_start_band_ch1.setSizePolicy(sizePolicy)
        self.spinBox_start_band_ch1.setMinimumSize(QSize(0, 0))
        self.spinBox_start_band_ch1.setMaximumSize(QSize(60, 16777215))
        self.spinBox_start_band_ch1.setMaximum(10000)
        self.spinBox_start_band_ch1.setValue(6500)

        self.gridLayout_2.addWidget(self.spinBox_start_band_ch1, 1, 1, 1, 1)

        self.label_3 = QLabel(self.groupBox)
        self.label_3.setObjectName(u"label_3")

        self.gridLayout_2.addWidget(self.label_3, 1, 2, 1, 1)

        self.spinBox_start_i_ch3 = QSpinBox(self.groupBox)
        self.spinBox_start_i_ch3.setObjectName(u"spinBox_start_i_ch3")
        sizePolicy.setHeightForWidth(self.spinBox_start_i_ch3.sizePolicy().hasHeightForWidth())
        self.spinBox_start_i_ch3.setSizePolicy(sizePolicy)
        self.spinBox_start_i_ch3.setMaximumSize(QSize(60, 16777215))

        self.gridLayout_2.addWidget(self.spinBox_start_i_ch3, 7, 3, 1, 1)

        self.radioButton_record_ch2 = QRadioButton(self.groupBox)
        self.radioButton_record_ch2.setObjectName(u"radioButton_record_ch2")
        sizePolicy.setHeightForWidth(self.radioButton_record_ch2.sizePolicy().hasHeightForWidth())
        self.radioButton_record_ch2.setSizePolicy(sizePolicy)
        self.radioButton_record_ch2.setMinimumSize(QSize(0, 0))
        self.radioButton_record_ch2.setMaximumSize(QSize(70, 16777215))
        font1 = QFont()
        font1.setKerning(False)
        self.radioButton_record_ch2.setFont(font1)
        self.radioButton_record_ch2.setLayoutDirection(Qt.RightToLeft)
        self.radioButton_record_ch2.setCheckable(True)
        self.radioButton_record_ch2.setAutoExclusive(False)

        self.gridLayout_2.addWidget(self.radioButton_record_ch2, 3, 3, 1, 1)

        self.radioButton_record_ch1 = QRadioButton(self.groupBox)
        self.radioButton_record_ch1.setObjectName(u"radioButton_record_ch1")
        sizePolicy1 = QSizePolicy(QSizePolicy.Preferred, QSizePolicy.Fixed)
        sizePolicy1.setHorizontalStretch(0)
        sizePolicy1.setVerticalStretch(0)
        sizePolicy1.setHeightForWidth(self.radioButton_record_ch1.sizePolicy().hasHeightForWidth())
        self.radioButton_record_ch1.setSizePolicy(sizePolicy1)
        self.radioButton_record_ch1.setMinimumSize(QSize(0, 0))
        self.radioButton_record_ch1.setMaximumSize(QSize(70, 16777215))
        self.radioButton_record_ch1.setTabletTracking(False)
        self.radioButton_record_ch1.setToolTipDuration(0)
        self.radioButton_record_ch1.setLayoutDirection(Qt.RightToLeft)
        self.radioButton_record_ch1.setAutoExclusive(False)

        self.gridLayout_2.addWidget(self.radioButton_record_ch1, 0, 3, 1, 1)

        self.radioButton_record_ch3 = QRadioButton(self.groupBox)
        self.radioButton_record_ch3.setObjectName(u"radioButton_record_ch3")
        sizePolicy.setHeightForWidth(self.radioButton_record_ch3.sizePolicy().hasHeightForWidth())
        self.radioButton_record_ch3.setSizePolicy(sizePolicy)
        self.radioButton_record_ch3.setMinimumSize(QSize(0, 0))
        self.radioButton_record_ch3.setMaximumSize(QSize(70, 16777215))
        self.radioButton_record_ch3.setLayoutDirection(Qt.RightToLeft)
        self.radioButton_record_ch3.setAutoExclusive(False)

        self.gridLayout_2.addWidget(self.radioButton_record_ch3, 6, 3, 1, 1)


        self.verticalLayout.addWidget(self.groupBox)

        self.verticalSpacer = QSpacerItem(20, 40, QSizePolicy.Minimum, QSizePolicy.Expanding)

        self.verticalLayout.addItem(self.verticalSpacer)

        self.groupBox_4 = QGroupBox(self.widget_2)
        self.groupBox_4.setObjectName(u"groupBox_4")
        self.verticalLayout_6 = QVBoxLayout(self.groupBox_4)
        self.verticalLayout_6.setSpacing(0)
        self.verticalLayout_6.setObjectName(u"verticalLayout_6")
        self.verticalLayout_6.setContentsMargins(6, 6, 6, 6)
        self.widget_4 = QWidget(self.groupBox_4)
        self.widget_4.setObjectName(u"widget_4")
        self.formLayout_5 = QFormLayout(self.widget_4)
        self.formLayout_5.setObjectName(u"formLayout_5")
        self.formLayout_5.setContentsMargins(6, 6, 6, 6)
        self.label_2 = QLabel(self.widget_4)
        self.label_2.setObjectName(u"label_2")

        self.formLayout_5.setWidget(0, QFormLayout.LabelRole, self.label_2)

        self.lineEdit_patient_id = QLineEdit(self.widget_4)
        self.lineEdit_patient_id.setObjectName(u"lineEdit_patient_id")
        self.lineEdit_patient_id.setMinimumSize(QSize(130, 0))

        self.formLayout_5.setWidget(0, QFormLayout.FieldRole, self.lineEdit_patient_id)

        self.label_7 = QLabel(self.widget_4)
        self.label_7.setObjectName(u"label_7")

        self.formLayout_5.setWidget(1, QFormLayout.LabelRole, self.label_7)

        self.spinBox_age = QSpinBox(self.widget_4)
        self.spinBox_age.setObjectName(u"spinBox_age")
        sizePolicy.setHeightForWidth(self.spinBox_age.sizePolicy().hasHeightForWidth())
        self.spinBox_age.setSizePolicy(sizePolicy)
        self.spinBox_age.setMinimumSize(QSize(130, 0))
        self.spinBox_age.setMaximum(122)

        self.formLayout_5.setWidget(1, QFormLayout.FieldRole, self.spinBox_age)

        self.label_5 = QLabel(self.widget_4)
        self.label_5.setObjectName(u"label_5")

        self.formLayout_5.setWidget(2, QFormLayout.LabelRole, self.label_5)

        self.comboBox_sex = QComboBox(self.widget_4)
        self.comboBox_sex.addItem("")
        self.comboBox_sex.addItem("")
        self.comboBox_sex.addItem("")
        self.comboBox_sex.setObjectName(u"comboBox_sex")
        sizePolicy.setHeightForWidth(self.comboBox_sex.sizePolicy().hasHeightForWidth())
        self.comboBox_sex.setSizePolicy(sizePolicy)
        self.comboBox_sex.setMinimumSize(QSize(130, 0))
        self.comboBox_sex.setEditable(False)

        self.formLayout_5.setWidget(2, QFormLayout.FieldRole, self.comboBox_sex)

        self.label_6 = QLabel(self.widget_4)
        self.label_6.setObjectName(u"label_6")

        self.formLayout_5.setWidget(3, QFormLayout.LabelRole, self.label_6)

        self.comboBox_habits = QComboBox(self.widget_4)
        self.comboBox_habits.addItem("")
        self.comboBox_habits.addItem("")
        self.comboBox_habits.addItem("")
        self.comboBox_habits.addItem("")
        self.comboBox_habits.setObjectName(u"comboBox_habits")
        sizePolicy.setHeightForWidth(self.comboBox_habits.sizePolicy().hasHeightForWidth())
        self.comboBox_habits.setSizePolicy(sizePolicy)
        self.comboBox_habits.setMinimumSize(QSize(130, 0))
        self.comboBox_habits.setEditable(False)

        self.formLayout_5.setWidget(3, QFormLayout.FieldRole, self.comboBox_habits)

        self.label = QLabel(self.widget_4)
        self.label.setObjectName(u"label")

        self.formLayout_5.setWidget(4, QFormLayout.LabelRole, self.label)

        self.spinBox_set_timer = QSpinBox(self.widget_4)
        self.spinBox_set_timer.setObjectName(u"spinBox_set_timer")
        self.spinBox_set_timer.setMinimumSize(QSize(130, 0))
        self.spinBox_set_timer.setMaximum(1000)

        self.formLayout_5.setWidget(4, QFormLayout.FieldRole, self.spinBox_set_timer)


        self.verticalLayout_6.addWidget(self.widget_4, 0, Qt.AlignVCenter)

        self.widget_3 = QWidget(self.groupBox_4)
        self.widget_3.setObjectName(u"widget_3")
        self.horizontalLayout = QHBoxLayout(self.widget_3)
        self.horizontalLayout.setObjectName(u"horizontalLayout")
        self.horizontalLayout.setContentsMargins(6, 6, 6, 6)
        self.pushButton_start_record = QPushButton(self.widget_3)
        self.pushButton_start_record.setObjectName(u"pushButton_start_record")

        self.horizontalLayout.addWidget(self.pushButton_start_record)

        self.pushButton_stop_record = QPushButton(self.widget_3)
        self.pushButton_stop_record.setObjectName(u"pushButton_stop_record")

        self.horizontalLayout.addWidget(self.pushButton_stop_record)

        self.pushButton_export = QPushButton(self.widget_3)
        self.pushButton_export.setObjectName(u"pushButton_export")

        self.horizontalLayout.addWidget(self.pushButton_export)


        self.verticalLayout_6.addWidget(self.widget_3)


        self.verticalLayout.addWidget(self.groupBox_4)

        self.radioButton = QRadioButton(self.widget_2)
        self.radioButton.setObjectName(u"radioButton")

        self.verticalLayout.addWidget(self.radioButton)


        self.horizontalLayout_2.addWidget(self.widget_2)

        self.groupBox1 = QGroupBox(self.centralwidget)
        self.groupBox1.setObjectName(u"groupBox1")
        sizePolicy2 = QSizePolicy(QSizePolicy.MinimumExpanding, QSizePolicy.Preferred)
        sizePolicy2.setHorizontalStretch(0)
        sizePolicy2.setVerticalStretch(0)
        sizePolicy2.setHeightForWidth(self.groupBox1.sizePolicy().hasHeightForWidth())
        self.groupBox1.setSizePolicy(sizePolicy2)
        self.gridLayout = QGridLayout(self.groupBox1)
        self.gridLayout.setObjectName(u"gridLayout")
        self.splitter_3 = QSplitter(self.groupBox1)
        self.splitter_3.setObjectName(u"splitter_3")
        self.splitter_3.setOrientation(Qt.Vertical)
        self.splitter = QSplitter(self.splitter_3)
        self.splitter.setObjectName(u"splitter")
        self.splitter.setOrientation(Qt.Horizontal)
        self.widget_5 = QWidget(self.splitter)
        self.widget_5.setObjectName(u"widget_5")
        self.widget_5.setMaximumSize(QSize(16777215, 16777215))
        self.verticalLayout_3 = QVBoxLayout(self.widget_5)
        self.verticalLayout_3.setObjectName(u"verticalLayout_3")
        self.verticalLayout_3.setContentsMargins(0, 0, 0, 0)
        self.label_8 = QLabel(self.widget_5)
        self.label_8.setObjectName(u"label_8")
        self.label_8.setMaximumSize(QSize(16777215, 25))
        self.label_8.setFont(font)

        self.verticalLayout_3.addWidget(self.label_8)

        self.pw_temp = PlotWidget(self.widget_5)
        self.pw_temp.setObjectName(u"pw_temp")
        sizePolicy3 = QSizePolicy(QSizePolicy.Expanding, QSizePolicy.Expanding)
        sizePolicy3.setHorizontalStretch(0)
        sizePolicy3.setVerticalStretch(0)
        sizePolicy3.setHeightForWidth(self.pw_temp.sizePolicy().hasHeightForWidth())
        self.pw_temp.setSizePolicy(sizePolicy3)
        self.pw_temp.setMaximumSize(QSize(16777215, 16777215))

        self.verticalLayout_3.addWidget(self.pw_temp)

        self.splitter.addWidget(self.widget_5)
        self.widget = QWidget(self.splitter)
        self.widget.setObjectName(u"widget")
        self.widget.setMaximumSize(QSize(16777215, 16777215))
        self.verticalLayout_5 = QVBoxLayout(self.widget)
        self.verticalLayout_5.setObjectName(u"verticalLayout_5")
        self.verticalLayout_5.setContentsMargins(0, 0, 0, 0)
        self.label_19 = QLabel(self.widget)
        self.label_19.setObjectName(u"label_19")
        self.label_19.setMaximumSize(QSize(16777215, 25))
        self.label_19.setFont(font)

        self.verticalLayout_5.addWidget(self.label_19)

        self.pw_temperature = PlotWidget(self.widget)
        self.pw_temperature.setObjectName(u"pw_sperometr")
        sizePolicy3.setHeightForWidth(self.pw_temperature.sizePolicy().hasHeightForWidth())
        self.pw_temperature.setSizePolicy(sizePolicy3)
        self.pw_temperature.setMaximumSize(QSize(16777215, 16777215))

        self.verticalLayout_5.addWidget(self.pw_temperature)

        self.splitter.addWidget(self.widget)
        self.splitter_3.addWidget(self.splitter)
        self.splitter_2 = QSplitter(self.splitter_3)
        self.splitter_2.setObjectName(u"splitter_2")
        self.splitter_2.setOrientation(Qt.Horizontal)
        self.widget_6 = QWidget(self.splitter_2)
        self.widget_6.setObjectName(u"widget_6")
        self.widget_6.setMaximumSize(QSize(16777215, 16777215))
        self.verticalLayout_4 = QVBoxLayout(self.widget_6)
        self.verticalLayout_4.setObjectName(u"verticalLayout_4")
        self.verticalLayout_4.setContentsMargins(0, 0, 0, 0)
        self.label_12 = QLabel(self.widget_6)
        self.label_12.setObjectName(u"label_12")
        self.label_12.setMinimumSize(QSize(0, 0))
        self.label_12.setMaximumSize(QSize(16777215, 25))
        self.label_12.setFont(font)

        self.verticalLayout_4.addWidget(self.label_12)

        self.pw_real_time = PlotWidget(self.widget_6)
        self.pw_real_time.setObjectName(u"pw_real_time")

        self.verticalLayout_4.addWidget(self.pw_real_time)

        self.splitter_2.addWidget(self.widget_6)
        self.widget_9 = QWidget(self.splitter_2)
        self.widget_9.setObjectName(u"widget_9")
        self.widget_9.setMaximumSize(QSize(16777215, 16777215))
        self.verticalLayout_7 = QVBoxLayout(self.widget_9)
        self.verticalLayout_7.setObjectName(u"verticalLayout_7")
        self.verticalLayout_7.setContentsMargins(0, 0, 0, 0)
        self.label_20 = QLabel(self.widget_9)
        self.label_20.setObjectName(u"label_20")
        self.label_20.setMaximumSize(QSize(16777215, 25))
        font2 = QFont()
        font2.setBold(True)
        font2.setItalic(False)
        self.label_20.setFont(font2)

        self.verticalLayout_7.addWidget(self.label_20)

        self.pw_humidity = PlotWidget(self.widget_9)
        self.pw_humidity.setObjectName(u"pw_calculated")
        sizePolicy3.setHeightForWidth(self.pw_humidity.sizePolicy().hasHeightForWidth())
        self.pw_humidity.setSizePolicy(sizePolicy3)
        self.pw_humidity.setMaximumSize(QSize(16777215, 16777215))

        self.verticalLayout_7.addWidget(self.pw_humidity)

        self.splitter_2.addWidget(self.widget_9)
        self.splitter_3.addWidget(self.splitter_2)

        self.gridLayout.addWidget(self.splitter_3, 0, 0, 1, 1)


        self.horizontalLayout_2.addWidget(self.groupBox1)

        MainWindow.setCentralWidget(self.centralwidget)
        self.menubar = QMenuBar(MainWindow)
        self.menubar.setObjectName(u"menubar")
        self.menubar.setGeometry(QRect(0, 0, 872, 24))
        self.menuHalitus_Studio = QMenu(self.menubar)
        self.menuHalitus_Studio.setObjectName(u"menuHalitus_Studio")
        self.menuHalitus_Studio.setEnabled(False)
        self.menuSettings = QMenu(self.menubar)
        self.menuSettings.setObjectName(u"menuSettings")
        self.menuAbout = QMenu(self.menubar)
        self.menuAbout.setObjectName(u"menuAbout")
        MainWindow.setMenuBar(self.menubar)
        self.statusbar = QStatusBar(MainWindow)
        self.statusbar.setObjectName(u"statusbar")
        MainWindow.setStatusBar(self.statusbar)

        self.menubar.addAction(self.menuHalitus_Studio.menuAction())
        self.menubar.addAction(self.menuSettings.menuAction())
        self.menubar.addAction(self.menuAbout.menuAction())

        self.retranslateUi(MainWindow)

        QMetaObject.connectSlotsByName(MainWindow)
    # setupUi

    def retranslateUi(self, MainWindow):
        MainWindow.setWindowTitle(QCoreApplication.translate("MainWindow", u"MainWindow", None))
#if QT_CONFIG(tooltip)
        self.centralwidget.setToolTip(QCoreApplication.translate("MainWindow", u"<html><head/><body><p>Halitus Studio</p></body></html>", None))
#endif // QT_CONFIG(tooltip)
        self.groupBox.setTitle(QCoreApplication.translate("MainWindow", u"Plot settings", None))
        self.label_9.setText(QCoreApplication.translate("MainWindow", u"Start band", None))
        self.label_21.setText(QCoreApplication.translate("MainWindow", u"Start I", None))
        self.label_11.setText(QCoreApplication.translate("MainWindow", u"Start band", None))
        self.label_23.setText(QCoreApplication.translate("MainWindow", u"Start I", None))
        self.label_15.setText(QCoreApplication.translate("MainWindow", u"Channel 1", None))
        self.label_24.setText(QCoreApplication.translate("MainWindow", u"End I", None))
        self.label_4.setText(QCoreApplication.translate("MainWindow", u"End I", None))
        self.label_18.setText(QCoreApplication.translate("MainWindow", u"Channel 2", None))
        self.label_10.setText(QCoreApplication.translate("MainWindow", u"End band", None))
        self.label_16.setText(QCoreApplication.translate("MainWindow", u"End band", None))
        self.label_17.setText(QCoreApplication.translate("MainWindow", u"Channel 3", None))
        self.label_14.setText(QCoreApplication.translate("MainWindow", u"Start band", None))
        self.label_13.setText(QCoreApplication.translate("MainWindow", u"End band", None))
        self.label_22.setText(QCoreApplication.translate("MainWindow", u"End I", None))
        self.label_3.setText(QCoreApplication.translate("MainWindow", u"Start I", None))
        self.radioButton_record_ch2.setText(QCoreApplication.translate("MainWindow", u"Record", None))
        self.radioButton_record_ch1.setText(QCoreApplication.translate("MainWindow", u"Record", None))
        self.radioButton_record_ch3.setText(QCoreApplication.translate("MainWindow", u"Record", None))
        self.groupBox_4.setTitle(QCoreApplication.translate("MainWindow", u"Measurement parameters", None))
        self.label_2.setText(QCoreApplication.translate("MainWindow", u"Patient ID", None))
        self.label_7.setText(QCoreApplication.translate("MainWindow", u"Age", None))
        self.label_5.setText(QCoreApplication.translate("MainWindow", u"Sex", None))
        self.comboBox_sex.setItemText(0, "")
        self.comboBox_sex.setItemText(1, QCoreApplication.translate("MainWindow", u"Male", None))
        self.comboBox_sex.setItemText(2, QCoreApplication.translate("MainWindow", u"Female", None))

        self.label_6.setText(QCoreApplication.translate("MainWindow", u"Habits", None))
        self.comboBox_habits.setItemText(0, "")
        self.comboBox_habits.setItemText(1, QCoreApplication.translate("MainWindow", u"Smoker", None))
        self.comboBox_habits.setItemText(2, QCoreApplication.translate("MainWindow", u"Alcoholic", None))
        self.comboBox_habits.setItemText(3, QCoreApplication.translate("MainWindow", u"Addict", None))

        self.label.setText(QCoreApplication.translate("MainWindow", u"Set timer", None))
        self.spinBox_set_timer.setSuffix(QCoreApplication.translate("MainWindow", u" Sec.", None))
        self.pushButton_start_record.setText(QCoreApplication.translate("MainWindow", u"Start", None))
        self.pushButton_stop_record.setText(QCoreApplication.translate("MainWindow", u"Stop", None))
        self.pushButton_export.setText(QCoreApplication.translate("MainWindow", u"Export", None))
        self.radioButton.setText(QCoreApplication.translate("MainWindow", u"Dark mode", None))
        self.label_8.setText(QCoreApplication.translate("MainWindow", u"Counter", None))
        self.label_19.setText(QCoreApplication.translate("MainWindow", u"Temperature, C", None))
        self.label_12.setText(QCoreApplication.translate("MainWindow", u"CO2 concentration, %", None))
        self.label_20.setText(QCoreApplication.translate("MainWindow", u"Humidity, %", None))
        self.menuHalitus_Studio.setTitle(QCoreApplication.translate("MainWindow", u"Halitus Studio", None))
        self.menuSettings.setTitle(QCoreApplication.translate("MainWindow", u"Settings", None))
        self.menuAbout.setTitle(QCoreApplication.translate("MainWindow", u"About", None))
    # retranslateUi

