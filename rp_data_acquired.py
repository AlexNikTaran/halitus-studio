import signal
import serial
import serial.tools.list_ports
import time
import numpy as np
import struct

class RpDataAcquired:
    _ser = None
    _data = []
    _pack_count = []
    _pulse_count = []
    _avg_value_channel_0 = []
    _avg_value_channel_1 = []
    _temp_bme = []
    _pressure_bme = []
    _humidity_bme = []

    def __init__(self, serial_addr):
        self.start = time.time()
        self._server_addr = serial_addr
        self._ser = serial.Serial(serial_addr, 460800)  # Adjust baud rate accordingly

    def close_thread(self):
        print("close_thread")

    def update(self):
        if self._ser is None:
            return
        # Read a line from the serial port
        line = self._ser.readline().decode('utf-8').strip()
        # Define the format string based on the original sprintf format
        format_string = "I;f;f;f;I;f;f"
        # Unpack the binary data using struct.unpack
        values_str = line.split(';')
        # Convert each split string value to a float
        values_floats = []
        for value in values_str:
            if value != '':
                values_floats.append(float(value))
        #values_floats = [float(value) for value in values_str]
        # Extracting values from the unpacked data
        self._temp_bme.append(values_floats[1])
        self._pressure_bme.append(values_floats[2])
        self._humidity_bme.append(values_floats[3])
        self._pulse_count.append(int(values_floats[4]))
        concentration = 10*(-np.log(float(values_floats[5])/float(values_floats[6])) + 0.44)/(1.32 + 0.42)
        self._avg_value_channel_0.append(concentration)
        #self._avg_value_channel_1.append(float(values[6]))
      #  print(f'Received data: {line}')

        return True

    def clear_data(self):
        self._data = []
        self._pack_count = []
        self._pulse_count = []
        self._avg_value_channel_0 = []
        self._avg_value_channel_1 = []
        self._temp_bme = []
        self._pressure_bme = []
        self._humidity_bme = []

    def get_avg_value_channel_0(self):
        return self._avg_value_channel_0

    def get_avg_value_channel_1(self):
        return self._avg_value_channel_1

    def get_pulse_count(self):
        return [x - self._pulse_count[0] for x in self._pulse_count]


    def get_temp_bme(self):
        return self._temp_bme

    def get_pressure_bme(self):
        return self._pressure_bme

    def get_humidity_bme(self):
        return self._humidity_bme
